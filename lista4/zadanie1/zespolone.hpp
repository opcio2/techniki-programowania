#ifndef ZESPOLONE_HPP
#define ZESPOLONE_HPP
#include <iostream>
#include <string>

class zespolone
{
    public:
        zespolone(int a, int b);
        virtual ~zespolone();

        int r();
        int i();

    private:
        int re;
        int im;
};
        zespolone dodaj(zespolone z1, zespolone z2);
        zespolone dodaj(zespolone z, int x);
        zespolone dodaj(int x, zespolone z);
        zespolone odejm(zespolone z1, zespolone z2);
        zespolone odejm(zespolone z, int x);
        zespolone odejm(int x, zespolone z);
        zespolone mnoz(zespolone z1, zespolone z2);
        zespolone mnoz(zespolone z, int x);
        zespolone mnoz(int x,zespolone z);


        zespolone operator+(zespolone z1, zespolone z2);
        zespolone operator+(zespolone z, int x);
        zespolone operator+(int x, zespolone z);
        zespolone operator-(zespolone z1, zespolone z2);
        zespolone operator-(zespolone z, int x);
        zespolone operator-(int x, zespolone z);
        zespolone operator*(zespolone z1, zespolone z2);
        zespolone operator*(zespolone z, int x);
        zespolone operator*(int x,zespolone z);

        std::ostream& operator<<(std::ostream& out, zespolone& p);
#endif // ZESPOLONE_HPP
