#include "zespolone.hpp"

zespolone::zespolone(int a, int b){
    re = a;
    im = b;
}

zespolone::~zespolone()
{
    //dtor
}

int zespolone::r()
{
    return re;
}

int zespolone::i()
{
    return im;
}


zespolone dodaj(zespolone z1, zespolone z2)
{
    return zespolone(z1.r()+z2.r(), z1.i()+z2.i());
}
zespolone dodaj(zespolone z, int x)
{
    return zespolone(z.r()+x, z.i());
}
zespolone dodaj(int x, zespolone z)
{
    return zespolone(z.r()+x, z.i());
}
zespolone odejm(zespolone z1, zespolone z2)
{
    return zespolone(z1.r()-z2.r(), z1.i()-z2.i());
}
zespolone odejm(zespolone z, int x)
{
    return zespolone(z.r()-x, z.i());
}
zespolone odejm(int x, zespolone z)
{
    return zespolone(z.r()-x, z.i());
}
zespolone mnoz(zespolone z1, zespolone z2)
{
   return zespolone((z1.r()*z2.r())-(z1.i()*z2.i()), (z1.r()*z2.i())+(z1.i()*z2.r()));
}
zespolone mnoz(zespolone z, int x)
{
    return zespolone(z.r()*x,z.i()*x);
}
zespolone mnoz(int x, zespolone z)
{
    return zespolone(z.r()*x,z.i()*x);
}
zespolone sprez(zespolone z)
{
    return zespolone(z.r(), -z.i());
}






zespolone operator+(zespolone z1, zespolone z2)
{
    return dodaj(z1,z2);
}
zespolone operator+(zespolone z, int x)
{
    return dodaj(z,x);
}
zespolone operator+(int x, zespolone z)
{
    return dodaj(x,z);
}
zespolone operator-(zespolone z1, zespolone z2)
{
    return odejm(z1,z2);
}
zespolone operator-(zespolone z, int x)
{
    return odejm(z,x);
}
zespolone operator-(int x, zespolone z)
{
    return odejm(x,z);
}
zespolone operator*(zespolone z1, zespolone z2)
{
        return mnoz(z1,z2);
}
zespolone operator*(int x, zespolone z)
{
    return mnoz(x,z);
}
zespolone operator*(zespolone z, int x)
{
    return mnoz(z,x);
}


std::ostream& operator<<(std::ostream& out, zespolone& p)
{
    return out << "[" << p.r() << "," << p.i() << "]";
}


