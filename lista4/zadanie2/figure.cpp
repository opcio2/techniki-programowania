#include "figure.hpp"

figure::figure()
{
    //ctor
}

figure::~figure()
{
    //dtor
}

circle::circle(double _r)
{
    r = _r;
}

circle::~circle()
{
    //dtor
}
triangle::triangle(double _a, double _b, double _c)
{
    a = _a;
    b = _b;
    c = _c;
    p = (_a+_b+_c)/2;
}

triangle::~triangle()
{
    //dtor
}
rectangle::rectangle(double _a, double _b)
{
    a = _a;
    b = _b;
}

rectangle::~rectangle()
{
    //dtor
}
double circle::area()
{
    return 3.141592*r*r;
}

double triangle::area()
{
    return  sqrt(p)*sqrt(p-a)*sqrt(p-b)*sqrt(p-c);
}

double rectangle::area()
{
    return a*b;
}

double figure::area()
{
    return 0;
}



std::ostream& circle::print(std::ostream& out)
{
    return out << "KOLO o promieniu " << r << " oraz polu " << area();
}

std::ostream& triangle::print(std::ostream& out)
{
    return out << "TROJKAT o bokach " << a << ", " << b << ", " << c << " oraz polu " << area();
}

std::ostream& rectangle::print(std::ostream& out)
{
    return out << "PROSTOKAT o bokach " << a << ", " << b << " oraz polu " << area();
}

std::ostream& figure::print(std::ostream& out)
{
    return out << "figura";
}



std::ostream& operator<<(std::ostream& out, figure& f)
{
    return f.print(out);
}

