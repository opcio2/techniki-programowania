#ifndef FIGURE_HPP
#define FIGURE_HPP
#include <string>
#include <iostream>
#include <math.h>

class figure
{
    public:

        figure();
        virtual ~figure();

        virtual double area();
        virtual std::ostream& print(std::ostream& out);



};

std::ostream& operator<<(std::ostream& out, figure& f);

class circle : public figure
{
    public:

        circle(double _r);
        virtual ~circle();

        virtual double area();
        virtual std::ostream& print(std::ostream& out);

    private:

        double r;
};

class triangle : public figure
{
    public:

        triangle(double _a, double _b, double _c);
        virtual ~triangle();

        virtual double area();
        virtual std::ostream& print(std::ostream& out);

    private:

        double a;
        double b;
        double c;
        double p;
};

class rectangle : public figure
{
    public:

        rectangle(double _a, double _b);
        virtual ~rectangle();

        virtual double area();
        virtual std::ostream& print(std::ostream& out);

    private:

        double a;
        double b;
};

#endif // FIGURE_HPP
