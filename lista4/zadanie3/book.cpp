#include <iostream>
#include <string>
#include <map>
#include <fstream>

using namespace std;

void print(map<string, string>& book);
void przepisz(map<string, string>& book);
void zapisz(map<string, string>& book);

int main()
{
    map<string, string> book;
    przepisz(book);

    cout << "KSIAZKA ADRESOWA" << endl;
    cout << "0 by zakonczyc" << endl;
    cout << "1 by dodac nowy adres" << endl;
    cout << "2 by zmienic adres" << endl;
    cout << "3 by usunac adres" << endl;
    cout << "4 by wyswietlic ksiazke" << endl;


    int t=1;
    string key;
    string val;

    while(t != 0){
        cout << "Wpisz numer" << endl;
        cin >> t;
        if(t==1 || t==2 || t==3 || t==4)
        {
            if(t==1)
            {
                cout << "Wprowadz nazwe" << endl;
                cin >> key;
                cout << "Wprowadz email" << endl;
                cin >> val;
                book[key] = val;
            }
             if(t==2)
            {
                cout << "Wprowadz nazwe" << endl;
                cin >> key;
                cout << "Wprowadz nowy email" << endl;
                cin >> val;
                book.erase(key);
                book[key] = val;
            }
             if(t==3)
            {
                cout << "Wprowadz nazwe" << endl;
                cin >> key;
                book.erase(key);
            }
             if(t==4)
            {
                print(book);
            }
        }
        else if(t != 0)
        {
            cout << "Wprowadzono zly numer. Sproboj ponownie" << endl;
        }
    }
    zapisz(book);
    return 0;

}

void przepisz(map<string, string>& book)
{
    ifstream plik("book.txt");
    if( plik.is_open())
    {
        string wiersz;
        int w=1;
        string key;

        while(getline( plik, wiersz ) )
        {
            if(w % 2 == 1)
            {
                key = wiersz;
            }
            else
            {
                book[key] = wiersz;
            }
            w++;
        }
    }
    else
    {
        cout << "blad otwarcia bliku ifstream" << endl;
    }

    plik.clear();
    plik.close();
}

void print(map<string, string>& book)
{
    ifstream plik("book.txt");

    if( plik.is_open())
    {
        string wiersz;
        int w=1;

        cout << "          NAZWA:" << "  MAIL         \n\n";

        for(map<string,string>::iterator itr=book.begin(), koniec=book.end(); itr!=book.end(); ++itr)
        {
            cout << "          " << itr->first << ":  " << itr->second << "\n\n";
        }
    }
    else
    {
        cout << "blad otwarcia bliku ifstream" << endl;
    }

    plik.clear();
    plik.close();

}

void zapisz(map<string, string>& book)
{
    ofstream plik("book.txt", ios::trunc|ios::out);

    if( plik.is_open())
    {
        for(map<string,string>::iterator itr=book.begin(), koniec=book.end(); itr!=book.end(); ++itr)
        {
            plik << itr->first << "\n" << itr->second << "\n";
        }
    }
    else
    {
        cout << "blad otwarcia bliku ifstream" << endl;
    }

    plik.clear();
    plik.close();
}
