#ifndef TICTACTOE_HPP
#define TICTACTOE_HPP
#include <iostream>

using namespace std;
class tictactoe
{
    public:

        tictactoe();
        virtual ~tictactoe();

        enum val{kolko, krzyzyk, puste};
        enum ruch{p1,p2};

        void game();
        void gamecpu();
        void min_max();
        void wstaw(int w, int k);
        void print();
        bool game_over();

    private:

        val tab[3][3];
        ruch a;

};

#endif // TICTACTOE_HPP
