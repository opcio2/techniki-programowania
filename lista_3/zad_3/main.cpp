#include <iostream>
#include "tictactoe.hpp"

using namespace std;

int main(){

    int gamemode;

    tictactoe game1;

    cout << "Jesli chcesz zagrac z komputerem, wprowadz numer 1, jesli chcesz przeprowadzic rozgrywke na dwoch graczy, wprowadz numer 2" << end;
    cin >> gamemode;

    while((gamemode!=1) && (gamemode!=2)){
        cout << "Nieprawidlowy numer, wprowadz numer 1, jesli chcesz przeprowadzic rozgrywke na dwoch graczy, wprowadz numer 2" << end;
        cin >> gamemode;
    }
    if(gamemode==1){
        game1.tictactoe::gamecpu();
    }
    else{
         game1.tictactoe::game();
    }

    return 0;
}
