#include <iostream>
#include "tictactoe.hpp"

using namespace std;

tictactoe::~tictactoe()
{
    //dtor
}




tictactoe::tictactoe()
{
    for(int i=0; i<3; i++){
        for(int j=0; j<3; j++){
            tab[i][j]= puste;
        }
    }

}


void tictactoe::game()
{

    int w, k;
    print();

    for(int t=1 ; t<=9 ; t++){

        if(t%2==1){
            a = p1;
            cout << "Wstaw kolko" << endl;
        }
        else{
                a = p2;
            cout << "Wstaw krzyzyk" << endl;
        }

        cout << "Numer wiersza: ";
        cin >> w;
        while((w!=1) && (w!=2) && (w!=3)){
            cout << "Nieieprawidlowy numer wiersza. Ponownie wprowadz numer." << endl;
            cin >> w;
        }

        cout << "Numer kolumny: ";
        cin >> k;
        while((k!=1) && (k!=2) && (k!=3)){
            cout << "Nieprawidlowy numer kolumny. Ponownie wprowadz numer." << endl;
            cin >> k;
        }

        w=w-1;
        k=k-1;

        if(tab[w][k]==puste){

            wstaw(w,k);
            print();

            if(game_over()){
                if(a==p1){
                    cout << "Wygralo kolko" << endl;
                }
                else{
                    cout << "Wygral krzyzyk" << endl;
                }
                t=10;
            }
            else if(t==9){
                cout << "Remis" << endl;
            }
        }
        else{
            cout << "Pole jest zajete, sproboj ponownie" << endl;
            t--;
        }
    }
}

void tictactoe::gamecpu()
{
    int b;

    cout << "Wpisz 1 jesli zaczyna gracz lub 2 jesli zaczyna komputer" << endl;
    cin >> b;

    while((b!=1) && (b!=2)){
        cout << "Nieprawid³owy numer, wprowadz ponownie 1 jesli zaczyna gracz lub 2 jesli zaczyna komputer" << endl;
        cin >> b;
    }

    int w, k;

    print();

    if(a==p1){
        for(int t=1 ; t<=9 ; t++){
            if(t%2==1){
                a = p1;
                cout << "Wstaw kolko" << endl;
                cout << "Numer wiersza: ";
                cin >> w;

                while((w!=1) && (w!=2) && (w!=3)){
                    cout << "Nieieprawidlowy numer wiersza. Ponownie wprowadz numer." << endl;
                    cin >> w;
                }

                cout << "Numer kolumny: ";
                cin >> k;

                while((k!=1) && (k!=2) && (k!=3)){
                    cout << "Nieprawidlowy numer kolumny. Ponownie wprowadz numer." << endl;
                    cin >> k;
                }

                w=w-1;
                k=k-1;

                if(tab[w][k]==puste){
                    wstaw(w,k);
                    print();
                }
            }
            else{
                    a = p2;
                    min_max();
                }


            if(game_over()){
                if(a==p1){
                    cout << "Wygral gracz" << endl;
                }
                else{
                    cout << "Wygral komputer" << endl;
                }
                t=10;
            }
            else if(t==9){
                cout << "Remis" << endl;
            }
            else{
            cout << "Pole jest zajete, sproboj ponownie" << endl;
            t--;
            }
        }
    }
    else{

    }

}


void tictactoe::wstaw(int w, int k)
{
    if(a==p1){
        tab[w][k]=kolko;
    }
    else{
        tab[w][k]=krzyzyk;
    }
}



void tictactoe::print()
{
    cout << "     1     2     3" <<endl;
    for(int o=0; o<=2; o++){
        cout << o+1;
        cout << "   ";
        for(int p=0; p<=2; p++){
            if(tab[o][p]==kolko){
                cout << "[O]";
            }
            else if(tab[o][p]==krzyzyk){
                cout << "[X]";
            }
            else{
                cout << "[ ]";
            }
            cout << "   ";
            if(p==2){
                cout << "" <<endl;
            }
        }
    }
}



bool tictactoe::game_over()
{
    for(int c=0; c<=2; c++){
        if(tab[c][0] != puste && tab[c][0]==tab[c][1] && tab[c][1]==tab[c][2]){
            return true;
        }
        else if(tab[0][c] != puste && tab[0][c]==tab[1][c] && tab[1][c]==tab[2][c]){
            return true;
        }
    }

    if(tab[0][0] != puste && tab[0][0]==tab[1][1] && tab[1][1]==tab[2][2]){
        return true;
    }
    if(tab[2][0] != puste && tab[2][0]==tab[1][1] && tab[1][1]==tab[0][2]){
        return true;
    }

    return false;
}

void tictactoe::min_max()
{

}
