#include "VectorInt.hpp"

VectorInt::VectorInt()
{
   tab  = new int[16];
   elements = new int[16];

   tab_size = 16;
   el_number = 0;
}

VectorInt::~VectorInt()
{
    delete[] tab;
    delete[] elements;

}

VectorInt::VectorInt(int x)
{
    tab  = new int[x];
    elements = new int[x];

    tab_size = x;
    el_number = 0;
}

VectorInt::VectorInt(const VectorInt &VectorInt)
{
    tab  = VectorInt.tab;
    elements = VectorInt.elements;
}

int VectorInt::At(int index)
{
    if(elements[index-1]==1){
        return tab[index-1];
    }
    else{
        return 99999;
    }
}

void VectorInt::Insert(int index, int value)
{
    tab[index-1]=value;
    if(elements[index-1]==0){
        el_number++;
    }
    elements[index-1]=1;
}

void VectorInt::Pushback(int element)
{
   Capacity();
   if(el_number==tab_size){
        int* pom = new int[tab_size];
        for(int j=0; j<tab_size; j++){
            pom[j] = tab[j];
        }

        delete[] tab;
        tab = new int[tab_size++];
        for(int k=0; k<tab_size; k++){
            tab[k] = pom[k];
        }

        tab[tab_size-1] = element;

        delete[] elements;
        elements = new int[tab_size];
        for(int l=0; l<tab_size; l++){
            elements[l] = 1;
        }
   }
   else{
        tab[el_number]=element;
        el_number++;
   }

}

void VectorInt::Popback()
{
    elements[el_number-1] = 0;
    el_number--;
}

void VectorInt::Shrinktofit()
{
    int* pom = new int[el_number];
    for(int i=0; i<el_number; i++){
        pom[i] = tab[i];
    }
    delete[] tab;
    int* tab = new int[el_number];
    for(int j=0; j<el_number; j++){
        tab[j] = pom[j];
    }
    tab_size = el_number;
}

void VectorInt::Clear()
{
    for(int i=0; i<el_number; i++){
        elements[i] = 0;
    }
    el_number = 0;
}


int VectorInt::Size()
{
    el_number = 0;
    for(int j=0; j<tab_size; j++){
        el_number =+ elements[j];
    }
    return el_number;
}

int VectorInt::Capacity()
{
    return tab_size;
}

std::ostream& operator<<(std::ostream& out, VectorInt& p, int el_number){
    if(el_number>0){
        std::ostream& operator<<(std::ostream& out, VectorInt& p, int el_number-1);
        return forout << "[" << p.tab[el_number-1] << "]";
    }
    else{
        return forout << "[" << p.tab[0] << "]";
    }
}
