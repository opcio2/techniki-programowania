#ifndef VECTORINT_HPP
#define VECTORINT_HPP


class VectorInt
{
    public:

        VectorInt();
        VectorInt(int x);
        VectorInt(const VectorInt &VectorInt);
        virtual ~VectorInt();

        int At(int index);
        void Insert(int index, int value);
        void Pushback(int element);
        void Popback();
        void Shrinktofit();
        void Clear();
        int Size();
        int Capacity();
    private:

        int tab_size;
        int el_number;
        int* tab;
        int* elements;
};

std::ostream& operator<<(std::ostream& out, VectorInt& p, int el_number);

#endif // VECTORINT_HPP
