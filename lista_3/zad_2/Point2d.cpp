#include "Point2d.hpp"
#include <iostream>
#include <cmath>
#define M_PI 4*atan(1.)



Point2d::Point2d()
    : _R(0)
    , _Phi(0){
}


Point2d::Point2d(double x, double y)
    : _R(x)
    , _Phi(y){
}


Point2d::Point2d(const Point2d& other)
    : _R(other._R)
    , _Phi(other._Phi){
}


Point2d& Point2d::operator= (const Point2d& other){
    this->_R = other._R;
    this->_Phi = other._Phi;

    return *this;
}

double Point2d::getX(){
    return cos(_Phi)*_R;
}

double Point2d::getY(){
    return sin(_Phi)*_R;
}

double Point2d::getR(){
    return _R;
}

double Point2d::getPhi(){
    return _Phi;
}

void Point2d::setXY(double x, double y){
    _R = sqrt(x*x+y*y);
    this->_Phi = atan(y/x);
}

void Point2d::setRPhi(double r, double phi){
    _R = r;
    _Phi = phi;
}

void Point2d::homothetia(double k)
{
   _R = k*_R;
   if(k<0){
        _Phi = -_Phi;
   }
   else if(k==0){
        _Phi = 0;
   }
}

void Point2d::rotation(double phi)
{
    _Phi =+ phi;
}
std::ostream& operator<<(std::ostream& out, Point2d& p){
    return out << "[" << p.getR() << ", " << p.getPhi() << "]";
}
